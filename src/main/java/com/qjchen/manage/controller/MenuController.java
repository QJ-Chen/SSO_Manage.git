package com.qjchen.manage.controller;

import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.MenuDTO;
import com.qjchen.common.model.entity.Menu;
import com.qjchen.common.model.vo.MenuVO;
import com.qjchen.common.utils.ConvertUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("${qjc.sso.api}/menu")
public class MenuController extends BusinessController<MenuVO, MenuDTO, Menu> {

    /**
     * 列表分页方法
     *
     * @param menuDTO 菜单DTO对象
     * @return 分页对象
     */
    @Override
    public Page<MenuVO> listPage(@RequestBody MenuDTO menuDTO) {
        int currentPageNo = menuDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            menuDTO.setCurrentPageNo(menuDTO.getPageSize()*menuDTO.getCurrentPageNo());
        }
        List<MenuDTO> list = super.serviceAdapter.list(menuDTO);
        List<MenuVO> menuVOS = ConvertUtil.listFusion(list, MenuVO.class);
        int count = super.serviceAdapter.count(menuDTO);
        return new Page<>(menuVOS.size()/menuDTO.getPageSize(),currentPageNo,menuDTO.getPageSize(),count,menuVOS);
    }

    /**
     * 列表
     *
     * @param menuDTO 当前菜单信息
     * @return 列表中的MenuVO对象集合
     */
    @Override
    public List<MenuVO> list(@RequestBody MenuDTO menuDTO) {
        List<MenuDTO> list = super.serviceAdapter.list(menuDTO);
        return ConvertUtil.listFusion(list, MenuVO.class);
    }
}
