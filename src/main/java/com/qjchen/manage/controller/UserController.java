package com.qjchen.manage.controller;

import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.interceptor.Result;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.annotations.ResModel;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.dto.UserDTO;
import com.qjchen.common.model.entity.User;
import com.qjchen.common.model.vo.AppVO;
import com.qjchen.common.model.vo.RoleVO;
import com.qjchen.common.model.vo.UserVO;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.sdk.server.QasServiceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("${qjc.sso.api}/user")
public class UserController extends BusinessController<UserVO, UserDTO, User> {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private RoleController roleController;

    @Autowired
    private AppController appController;

    @Override
    public Page<UserVO> listPage(@RequestBody UserDTO userDTO) {
        int currentPageNo = userDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            userDTO.setCurrentPageNo(userDTO.getPageSize()*userDTO.getCurrentPageNo());
        }
        List<UserVO> userVOList = new ArrayList<>();
        super.serviceAdapter.list(userDTO).forEach(userDTOTemp -> {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(userDTOTemp,userVO);

            RoleVO roleVO = roleController.get(new RoleDTO(userDTOTemp.getRoleId()));
            userVO.setRoleVO(roleVO);

            List<AppVO> list = appController.list(new AppDTO(userDTOTemp.getAppIds()));
            userVO.setAppVOList(list);

            userVOList.add(userVO);
        });
        int count = super.serviceAdapter.count(userDTO);
        return new Page<>(userVOList.size()/userDTO.getPageSize(),currentPageNo,userDTO.getPageSize(),count,userVOList);
    }

    @Override
    public UserVO get(@RequestBody UserDTO userDTO) {
        UserDTO userDTOTemp = super.serviceAdapter.get(userDTO);
        RoleVO roleVO = roleController.get(new RoleDTO(userDTOTemp.getRoleId()));
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userDTOTemp,userVO);
        userVO.setRoleVO(roleVO);
        return userVO;
    }

    @ResModel
    @PostMapping("/getByToken")
    public UserVO getByToken(HttpServletRequest request){
        return QasServiceContext.getUserService.getByToken(request);
    }

    @PostMapping("/logOut")
    public Result logout(HttpServletRequest request){
        return QasServiceContext.getUserService.logOut(request);
    }

}
