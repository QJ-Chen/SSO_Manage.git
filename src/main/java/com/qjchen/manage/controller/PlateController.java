package com.qjchen.manage.controller;

import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.model.Map;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.annotations.ResModel;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.common.model.dto.PlateDTO;
import com.qjchen.common.model.entity.Plate;
import com.qjchen.common.model.enums.PlateType;
import com.qjchen.common.model.vo.AppVO;
import com.qjchen.common.model.vo.PlateVO;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.manage.model.vo.PlateTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("${qjc.sso.api}/plate")
public class PlateController extends BusinessController<PlateVO, PlateDTO,Plate> {

    @Autowired
    private AppController appController;

    @Override
    public Page<PlateVO> listPage(@RequestBody PlateDTO plateDTO) {
        int currentPageNo = plateDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            plateDTO.setCurrentPageNo(plateDTO.getPageSize()*plateDTO.getCurrentPageNo());
        }

        List<PlateVO> plateVOS = new ArrayList<>();
        super.serviceAdapter.list(plateDTO).forEach(plateDTOTemp -> {
            PlateVO fusion = ConvertUtil.fusion(plateDTOTemp, PlateVO.class);
            fusion.setPlateType(plateDTOTemp.getPlateType());
            fusion.setStatus(plateDTOTemp.getStatus());

            plateVOS.add(fusion);
        });

        int count = super.serviceAdapter.count(plateDTO);

        return new Page<>(plateVOS.size()/plateDTO.getPageSize(),currentPageNo,plateDTO.getPageSize(),count,plateVOS);
    }

    @ResModel
    @GetMapping("/listType")
    public List<Map> listType(){
        List<Map> list = new ArrayList<>();
        for (PlateType plateType : PlateType.values()) {
            Map map = new Map(plateType.getPlateType(),plateType.getTypeName());
            list.add(map);
        }
        return list;
    }

    @Override
    public List<PlateVO> list(@RequestBody PlateDTO plateDTO) {
        List<PlateDTO> plateDTOList = super.serviceAdapter.list(plateDTO);
        List<PlateVO> plateVOS = new ArrayList<>();
        plateDTOList.forEach(plateDTOTemp -> {
            PlateVO fusion = ConvertUtil.fusion(plateDTOTemp, PlateVO.class);
            fusion.setPlateType(plateDTOTemp.getPlateType());
            fusion.setStatus(plateDTOTemp.getStatus());
            plateVOS.add(fusion);
        });
        return plateVOS;
    }

    @Override
    public PlateVO get(@RequestBody PlateDTO plateDTO) {
        PlateDTO plateDTOTemp = super.serviceAdapter.get(plateDTO);
        PlateVO fusion = ConvertUtil.fusion(plateDTOTemp, PlateVO.class);
        fusion.setPlateType(plateDTOTemp.getPlateType());
        fusion.setStatus(plateDTOTemp.getStatus());
        return fusion;
    }
}
