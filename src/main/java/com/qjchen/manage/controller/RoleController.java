package com.qjchen.manage.controller;

import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.model.EditInfo;
import com.qjchen.common.model.Page;
import com.qjchen.common.config.RedisConstants;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.entity.Role;
import com.qjchen.common.model.vo.RoleVO;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.common.utils.ResUtils;
import com.qjchen.manage.utils.jedis.RedisPoolUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色控制层
 * @author qjchen
 * @date 2023.7.29
 */
@RestController
@RequestMapping("${qjc.sso.api}/role")
public class RoleController extends BusinessController<RoleVO, RoleDTO, Role> {

    @Override
    public Page<RoleVO> listPage(@RequestBody RoleDTO roleDTO) {
        int currentPageNo = roleDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            roleDTO.setCurrentPageNo(roleDTO.getPageSize()*roleDTO.getCurrentPageNo());
        }
        List<RoleDTO> list = super.serviceAdapter.list(roleDTO);
        List<RoleVO> roleVOList = ConvertUtil.listFusion(list, RoleVO.class);
        int count = super.serviceAdapter.count(roleDTO);
        return new Page<>(roleVOList.size()/roleDTO.getPageSize(),currentPageNo,roleDTO.getPageSize(),count,roleVOList);
    }

    @Override
    public List<RoleVO> list(@RequestBody RoleDTO roleDTO) {
        if (!RedisPoolUtil.heixists(RedisConstants.SSO_MANAGE_REDIS_TEMP,"role_list_temp")){
            List<RoleDTO> list = super.serviceAdapter.list(roleDTO);

            Map<String,String> roleVOMap = new HashMap<>();
            roleVOMap.put("role_list_temp",JSONObject.toJSONString(ConvertUtil.listFusion(list, RoleVO.class)));
            RedisPoolUtil.hmset(RedisConstants.SSO_MANAGE_REDIS_TEMP,roleVOMap);

            return ConvertUtil.listFusion(list, RoleVO.class);
        }else{
            return RedisPoolUtil.hget(RedisConstants.SSO_MANAGE_REDIS_TEMP, "role_list_temp", List.class);
        }
    }

    @Override
    public RoleVO get(@RequestBody RoleDTO roleDTO) {
        if (!RedisPoolUtil.heixists(RedisConstants.SSO_MANAGE_REDIS_TEMP,"role_temp"+roleDTO.getId())){
            RoleDTO roleDTOTemp = super.serviceAdapter.get(roleDTO);

            Map<String,String> roleVOMap = new HashMap<>();
            roleVOMap.put("role_temp"+roleDTO.getId(),JSONObject.toJSONString(ConvertUtil.fusion(roleDTOTemp, RoleVO.class)));
            RedisPoolUtil.hmset(RedisConstants.SSO_MANAGE_REDIS_TEMP,roleVOMap);

            return ConvertUtil.fusion(roleDTOTemp, RoleVO.class);
        }else{
            return RedisPoolUtil.hget(RedisConstants.SSO_MANAGE_REDIS_TEMP,"role_temp"+roleDTO.getId(), RoleVO.class);
        }
    }

    @Override
    public String update(@RequestBody RoleDTO roleDTO) {
        boolean update = super.serviceAdapter.update(roleDTO);
        if(update){
            List<RoleDTO> list = super.serviceAdapter.list(new RoleDTO());
            Map<String,String> roleVOMap = new HashMap<>();
            roleVOMap.put("role_list_temp",JSONObject.toJSONString(ConvertUtil.listFusion(list, RoleVO.class)));
            RedisPoolUtil.hmset(RedisConstants.SSO_MANAGE_REDIS_TEMP,roleVOMap);
        }
        return ResUtils.isEdit(update, EditInfo.UPDATENUM);
    }

    @Override
    public String insert(@RequestBody RoleDTO roleDTO) {
        boolean insert = super.serviceAdapter.insert(roleDTO);
        return ResUtils.isEdit(insert, EditInfo.INSERTNUM);
    }
}
