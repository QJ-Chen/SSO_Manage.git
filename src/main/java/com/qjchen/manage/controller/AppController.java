package com.qjchen.manage.controller;

import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.model.Page;
import com.qjchen.common.config.RedisConstants;
import com.qjchen.common.model.annotations.ResModel;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.common.model.dto.PlateDTO;
import com.qjchen.common.model.dto.UserDTO;
import com.qjchen.common.model.entity.App;
import com.qjchen.common.model.vo.AppVO;
import com.qjchen.common.model.vo.PlateVO;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.manage.service.PlateService;
import com.qjchen.manage.service.impl.UserServiceImpl;
import com.qjchen.manage.utils.jedis.RedisPoolUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${qjc.sso.api}/app")
public class AppController extends BusinessController<AppVO, AppDTO, App> {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private PlateService plateService;

    @Override
    public Page<AppVO> listPage(@RequestBody AppDTO appDTO) {
        int currentPageNo = appDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            appDTO.setCurrentPageNo(appDTO.getPageSize()*appDTO.getCurrentPageNo());
        }
        List<AppVO> appVOList = new ArrayList<>();
        super.serviceAdapter.list(appDTO).forEach(appDTOTemp -> {
            UserDTO userDTO = userService.get(new UserDTO(Integer.valueOf(appDTOTemp.getCreatedBy())));
            AppVO appVO = ConvertUtil.fusion(appDTOTemp, AppVO.class);
            appVO.setCreatedBy(userDTO.getUsername());

            PlateDTO plateDTO = plateService.get(new PlateDTO(appDTOTemp.getPlateId()));
            appVO.setPlate(ConvertUtil.fusion(plateDTO, PlateVO.class));
            appVOList.add(appVO);
        });
        int count = super.serviceAdapter.count(appDTO);
        return new Page<>(appVOList.size()/appDTO.getPageSize(),currentPageNo,appDTO.getPageSize(),count,appVOList);
    }

    @Override
    public List<AppVO> list(@RequestBody AppDTO appDTO) {
        String splitIds = "app_list_temp_"+(appDTO.getIds()==null?"all":JSONObject.toJSONString(appDTO.getIds()));
        if (!RedisPoolUtil.heixists(RedisConstants.SSO_MANAGE_REDIS_TEMP,splitIds)){
            List<AppDTO> list = super.serviceAdapter.list(appDTO);

            List<AppVO> appVOList = new ArrayList<>();
            list.forEach(appDTOTemp -> {
                AppVO appVO = new AppVO();
                appVO.setId(appDTOTemp.getId());
                appVO.setAppName(appDTOTemp.getAppName());
                appVOList.add(appVO);
            });

            Map<String,String> appMap = new HashMap<>();
            appMap.put(splitIds,JSONObject.toJSONString(appVOList));
            RedisPoolUtil.hmset(RedisConstants.SSO_MANAGE_REDIS_TEMP,appMap);

            return appVOList;
        }else{
            return RedisPoolUtil.hget(RedisConstants.SSO_MANAGE_REDIS_TEMP,splitIds,List.class);
        }
    }

    @Override
    public String insert(@RequestBody AppDTO appDTO) {
        String signature = DigestUtils.sha256Hex(appDTO.getId() + appDTO.getAppName());
        appDTO.setAppSignature(signature);
        return super.insert(appDTO);
    }

    @ResModel
    @PostMapping("/listAppName")
    public List<String> listAppName(@RequestBody AppDTO appDTO){
        List<AppDTO> list = super.serviceAdapter.list(appDTO);
        return list.stream().map(AppDTO::getAppName).collect(Collectors.toList());
    }
}
