package com.qjchen.manage.service;

import com.qjchen.common.api.service.BusinessService;
import com.qjchen.common.model.dto.MenuDTO;

public interface MenuService extends BusinessService<MenuDTO> {
}
