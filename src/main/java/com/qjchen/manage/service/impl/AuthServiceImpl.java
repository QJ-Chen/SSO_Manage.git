package com.qjchen.manage.service.impl;

import com.qjchen.common.api.service.BusinessServiceAdapter;
import com.qjchen.common.model.dto.AuthDTO;
import com.qjchen.common.model.entity.Auth;
import com.qjchen.manage.service.AuthService;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl extends BusinessServiceAdapter<Auth, AuthDTO> implements AuthService {
}
