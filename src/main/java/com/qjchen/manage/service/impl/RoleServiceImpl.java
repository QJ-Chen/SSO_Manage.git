package com.qjchen.manage.service.impl;

import com.qjchen.common.api.service.BusinessServiceAdapter;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.entity.Role;
import com.qjchen.manage.service.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends BusinessServiceAdapter<Role,RoleDTO> implements RoleService {


}
