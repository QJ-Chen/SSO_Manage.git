package com.qjchen.manage.service.impl;

import com.qjchen.common.api.service.BusinessServiceAdapter;
import com.qjchen.common.model.dto.MenuDTO;
import com.qjchen.common.model.entity.Menu;
import com.qjchen.manage.service.MenuService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl extends BusinessServiceAdapter<Menu, MenuDTO> implements MenuService {
}
