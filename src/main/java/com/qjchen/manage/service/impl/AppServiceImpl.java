package com.qjchen.manage.service.impl;


import com.qjchen.common.api.service.BusinessServiceAdapter;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.common.model.entity.App;
import com.qjchen.manage.service.AppService;
import org.springframework.stereotype.Service;


@Service
public class AppServiceImpl extends BusinessServiceAdapter<App, AppDTO> implements AppService {

}
