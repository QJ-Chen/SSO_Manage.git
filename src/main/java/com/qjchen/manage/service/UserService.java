package com.qjchen.manage.service;

import com.qjchen.common.api.service.BusinessService;
import com.qjchen.common.model.dto.UserDTO;

public interface UserService extends BusinessService<UserDTO> {
}
