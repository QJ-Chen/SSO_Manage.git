package com.qjchen.manage.service;


import com.qjchen.common.api.service.BusinessService;
import com.qjchen.common.model.dto.PlateDTO;

public interface PlateService extends BusinessService<PlateDTO> {
}
