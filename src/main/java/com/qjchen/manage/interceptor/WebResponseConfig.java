package com.qjchen.manage.interceptor;

import com.qjchen.qas.interceptor.AuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class WebResponseConfig implements WebMvcConfigurer {

    @Bean
    public RequestHandler requestHandler(){
        return new RequestHandler();
    }

    /**
     * 权限拦截器
     */
    @Bean
    public AuthInterceptor authInterceptor() {
        return new AuthInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor()).addPathPatterns("/**").excludePathPatterns("classpath:/static/**").order(2);
        registry.addInterceptor(requestHandler()).addPathPatterns("/**").excludePathPatterns("classpath:/static/**").order(3);
    }


    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.removeIf(e -> e.getClass().isAssignableFrom(StringHttpMessageConverter.class));
    }

}
