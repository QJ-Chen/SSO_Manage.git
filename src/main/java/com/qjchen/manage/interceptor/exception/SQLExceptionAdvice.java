package com.qjchen.manage.interceptor.exception;

import com.qjchen.common.interceptor.Result;
import com.qjchen.common.interceptor.ResultEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

@RestControllerAdvice
public class SQLExceptionAdvice {

    private static final Logger log = LoggerFactory.getLogger(SQLExceptionAdvice.class);

    @ExceptionHandler({SQLException.class})
    public Result<?> sqlException(SQLException e){
        log.error("数据库错误！",e);
        return Result.failed(ResultEnum.COMMON_FAILED.getCode(), "数据库错误！");
    }
}
