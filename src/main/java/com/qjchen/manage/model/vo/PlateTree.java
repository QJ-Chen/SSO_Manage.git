package com.qjchen.manage.model.vo;

import com.qjchen.common.model.Map;
import com.qjchen.common.model.vo.AppVO;

import java.util.List;

public class PlateTree {

    private Integer id;

    private String plateCode;

    private String plateName;

    private Map status;

    private Map plateType;

    private List<AppVO> appVOList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlateCode() {
        return plateCode;
    }

    public void setPlateCode(String plateCode) {
        this.plateCode = plateCode;
    }

    public String getPlateName() {
        return plateName;
    }

    public void setPlateName(String plateName) {
        this.plateName = plateName;
    }

    public Map getStatus() {
        return status;
    }

    public void setStatus(Map status) {
        this.status = status;
    }

    public Map getPlateType() {
        return plateType;
    }

    public void setPlateType(Map plateType) {
        this.plateType = plateType;
    }

    public List<AppVO> getAppVOList() {
        return appVOList;
    }

    public void setAppVOList(List<AppVO> appVOList) {
        this.appVOList = appVOList;
    }
}
