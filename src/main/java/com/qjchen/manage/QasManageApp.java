package com.qjchen.manage;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@MapperScan("com.qjchen.manage.mapper")
@ComponentScan({"com.qjchen"})
@SpringBootApplication
public class QasManageApp extends SpringBootServletInitializer {

    private static final Logger log = LoggerFactory.getLogger(QasManageApp.class);

    /**
     * 用于打包war包，可部署至外部tomcat/jetty等容器中
     * @param builder a builder for the application context
     * @return SpringApplicationBuilder
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(QasManageApp.class);
    }

    public static void main(String[] args) {
        String javaVersion = System.getProperty("java.version");
        System.setProperty("java.version.banner", javaVersion);
        ConfigurableApplicationContext application = SpringApplication.run(QasManageApp.class, args);
        Environment env = application.getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 '{}' 启动成功!\n\t" +
                        "java版本号："+ javaVersion + "\n\t"+
                        "SpringBoot版本号："+ SpringApplication.class.getPackage().getImplementationVersion() +"\n\t"+
                        "访问端口："+ env.getProperty("server.port") +"\n" +
                        "----------------------------------------------------------",
                env.getProperty("qjc.sso.client-app-code"));
    }

}
