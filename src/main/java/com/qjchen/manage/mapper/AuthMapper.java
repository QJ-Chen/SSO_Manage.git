package com.qjchen.manage.mapper;

import com.qjchen.common.api.mapper.BusinessMapper;
import com.qjchen.common.model.dto.AuthDTO;
import com.qjchen.common.model.entity.Auth;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthMapper extends BusinessMapper<Auth, AuthDTO> {
}
