package com.qjchen.manage.mapper;

import com.qjchen.common.api.mapper.BusinessMapper;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.entity.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleMapper extends BusinessMapper<Role, RoleDTO> {
}
