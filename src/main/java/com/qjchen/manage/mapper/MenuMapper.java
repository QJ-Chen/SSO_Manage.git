package com.qjchen.manage.mapper;

import com.qjchen.common.api.mapper.BusinessMapper;
import com.qjchen.common.model.dto.MenuDTO;
import com.qjchen.common.model.entity.Menu;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuMapper extends BusinessMapper<Menu, MenuDTO> {
}
