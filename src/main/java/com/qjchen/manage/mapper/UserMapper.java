package com.qjchen.manage.mapper;


import com.qjchen.common.api.mapper.BusinessMapper;
import com.qjchen.common.model.dto.UserDTO;
import com.qjchen.common.model.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BusinessMapper<User, UserDTO> {

}
