package com.qjchen.manage;

import com.qjchen.common.model.dto.PlateDTO;
import com.qjchen.manage.service.PlateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = QasManageApp.class)
public class QasTestMain {

    @Autowired
    private PlateService plateService;
    @Test
    public void testUUID(){
        PlateDTO plateDTO = plateService.get(new PlateDTO(1));
        System.out.println(plateDTO);
    }
}
