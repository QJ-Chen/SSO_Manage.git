# QAS统一登录平台Manage:管理中心(后端)

项目环境:

* 模块基于jdk_1.8开发，支持maven或gradle构建。
* 该模块主要为官方提供管理中心后端。
# 项目简介

### 作者：QJ·Chen

项目基于Spring系列开发，主要用于单点登录，统一用户数据管理。作为毕业设计使用。
### 项目地址
* [Common:公共组件，提供公用模型、依赖、工具。](https://gitee.com/QJ-Chen/SSO_Common.git)
* [Service:服务端，提供登录授权功能。](https://gitee.com/QJ-Chen/SSO_Service.git)
* [Client:客户端，提供给开发者接入QAS，提供默认拦截器功能(接入必须)](https://gitee.com/QJ-Chen/SSO_Client.git)
* [SDK:第三方接入模块，提供给第三方开发者获取服务端数据用，(强烈建议)](https://gitee.com/QJ-Chen/SSO_SDK.git)
* [Manage:官方提供管理中心后端](https://gitee.com/QJ-Chen/SSO_Manage.git)
* [Web:官方提供管理中心前端](https://gitee.com/QJ-Chen/SSO_Web.git)

### 构建
* Maven:3.5.3
* Gradle:7.6.2

### 依赖
  ```groovy
  implementation ('org.springframework.boot:spring-boot-starter-web:2.7.7') {
    exclude group: 'org.springframework.boot', module: 'spring-boot-starter-tomcat'
  }
  implementation ('org.springframework.boot:spring-boot-starter-aop:2.7.7')
  implementation ('org.springframework.boot:spring-boot-starter-jetty:2.7.7')
  implementation ('org.mybatis.spring.boot:mybatis-spring-boot-starter:2.3.0')
  implementation ('com.zaxxer:HikariCP:4.0.3')
  implementation ('mysql:mysql-connector-java:8.0.13')
  implementation ('com.qjchen:QJChen_SSO_Client:1.0.0')
  implementation ('com.qjchen:QJChen_SSO_SDK:1.0.0')
  implementation ('org.springframework.boot:spring-boot-starter-data-redis:2.7.14')
  implementation ('redis.clients:jedis:4.2.2')
  testImplementation ('junit:junit:4.13.1')
 ```

### 更新时间
* 2023年10月13日：QJ·Chen

